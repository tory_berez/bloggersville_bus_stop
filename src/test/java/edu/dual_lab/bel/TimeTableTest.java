package edu.dual_lab.bel;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TimeTableTest {
    private  TimeTable timeTable;

    @Before
    public void createInitialTimeTable(){
        BusService bus1 = new BusService("Posh", "10:15", "10:50");
        BusService bus2 = new BusService("Posh", "10:25", "11:10");
        this.timeTable = new TimeTable();
        timeTable.offerService(bus1);
        timeTable.offerService(bus2);

    }

    @Test
    public void offerInefficientServiceTest()
    {
        int initialServicesAmount = this.timeTable.getAllServices().size();
        BusService inefficientBus = new BusService("Posh", "10:05", "10:50");
        timeTable.offerService(inefficientBus);
        assertEquals(initialServicesAmount,this.timeTable.getAllServices().size());
    }

    @Test
    public void offerEfficientServiceTest()
    {
        int initialServicesAmount = this.timeTable.getAllServices().size();
        BusService inefficientBus = new BusService("Posh", "10:05", "10:40");
        timeTable.offerService(inefficientBus);
        assertEquals(initialServicesAmount + 1,this.timeTable.getAllServices().size());
    }

    @Test
    public void offerSameGrottyServiceTest()
    {
        int initialServicesAmount = this.timeTable.getAllServices().size();
        BusService sameGrotty = new BusService("Grotty", "10:15", "10:50");
        timeTable.offerService(sameGrotty);
        assertEquals(initialServicesAmount,this.timeTable.getAllServices().size());
        for(BusService service:timeTable.getAllServices()){
            assertEquals(service.getName(), "Posh");
        }
    }
}
