package edu.dual_lab.bel;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BusServiceTest {

    @Test
    public void calcDurationTest()
    {
        BusService bus = new BusService("Grotty", "10:15","10:45");
        assertEquals(bus.getTripDuration(), 1800);
    }
}
