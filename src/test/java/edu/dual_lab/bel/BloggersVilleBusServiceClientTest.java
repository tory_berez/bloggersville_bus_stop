package edu.dual_lab.bel;

import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class BloggersVilleBusServiceClientTest {

    @Test
    public void getBusServiceFromStringTest() throws InvalidInputException{
        String busString = "Posh 10:15 11:10";
        BusService manualBus = new BusService("Posh", "10:15","11:10");
        BusService bus = BloggersVilleBusServiceClient.getBusServiceFromString(busString);
        assertNotNull(bus);
        assertEquals(manualBus,bus);

    }

    @Test
    public void getTimeTableFromFileTest() throws InvalidInputException, IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("timeTable.txt").getFile());
        TimeTable timeTable = BloggersVilleBusServiceClient.getTimeTableFromFile(file.getAbsolutePath());
        assertEquals(6, timeTable.getAllServices().size());
    }
}
