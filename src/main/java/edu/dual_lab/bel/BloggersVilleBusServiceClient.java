package edu.dual_lab.bel;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class BloggersVilleBusServiceClient {
    private static String outputFileName = "output.txt";
    private static String poshName = "Posh";
    private static String grottyName = "Grotty";


    public static TimeTable getTimeTableFromFile(String filePath) throws InvalidInputException,IOException{
        TimeTable jointTimeTable = new TimeTable();
        try (Scanner scan = new Scanner(Paths.get(filePath))) {
            while (scan.hasNextLine()) {
                String line = scan.nextLine();
                BusService service = getBusServiceFromString(line);
                jointTimeTable.offerService(service);
            }
        }

        return jointTimeTable;
    }

    private static Path getOutputPathInSameDir(String filePath, String outputFileName) {
        String directoryName = new File(filePath).getParent();
        return Paths.get(directoryName, outputFileName);
    }

    public static void writeToFileWithSameDir(String existingFilePath, TimeTable timeTable) throws IOException {
        Path outputFile = getOutputPathInSameDir(existingFilePath, outputFileName);
        Set<BusService> allServices = timeTable.getAllServices();
        Set<BusService> posh = getServicesWithName(allServices, poshName);
        Set<BusService> grotty = getServicesWithName(allServices, grottyName);
        try (BufferedWriter writer = Files.newBufferedWriter(outputFile)) {
            for (BusService bus : posh) {
                writer.write(bus.toString());
                writer.newLine();
            }

            writer.newLine();
            for (BusService bus : grotty) {
                writer.write(bus.toString());
                writer.newLine();
            }
        }
    }

    private static Set<BusService> getServicesWithName(Set<BusService> timeTable, String name) {
        Set<BusService> posh = new TreeSet<>();
        for (BusService bus : timeTable) {
            if (bus.getName().equals(name)) {
                posh.add(bus);
            }
        }

        return posh;
    }

    public static BusService getBusServiceFromString(String originalString) throws InvalidInputException{
        String[] serviceInfo = originalString.split(" ");
        if (serviceInfo.length < 3) {
            throw new InvalidInputException("The provided line did not contain all required fields;\n" + originalString);
        }

        BusService service = new BusService(serviceInfo[0], serviceInfo[1], serviceInfo[2]);
        service.setOriginalString(originalString);
        return service;
    }
}
