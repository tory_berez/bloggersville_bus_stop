package edu.dual_lab.bel;

public class InvalidInputException extends Exception {

    private static String invalidInput = "The input from the file is invalid\n";

    public InvalidInputException() {
        super();
    }

    public InvalidInputException(String message) {
        super(invalidInput + message);
    }
}
