package edu.dual_lab.bel;

import java.util.*;
import java.util.function.BiPredicate;

public class TimeTable {

    private Set<BusService> allServices;
    private static final String lessPreferredService = "Grotty";

    public TimeTable() {
        this.allServices = new HashSet();
    }

    private BiPredicate<BusService, BusService> moreEfficient = (first, second) -> (
            ((first.getStartTime().compareTo(second.getStartTime()) < 0) &&
                    (first.getEndTime().compareTo(second.getEndTime()) >= 0))
                    ||
                    (first.getStartTime().compareTo(second.getStartTime()) == 0) &&
                    (first.getEndTime().compareTo(second.getEndTime()) > 0));

    private BiPredicate<BusService, BusService> sameGrotty = (first, second) -> (
            ((first.getStartTime().compareTo(second.getStartTime()) == 0) &&
                    (first.getEndTime().compareTo(second.getEndTime()) == 0))&&
    first.getName().equals(lessPreferredService));

    private BiPredicate<BusService, BusService> lessEfficient = (first, second) -> (
            ((first.getStartTime().compareTo(second.getStartTime()) > 0) &&
                    (first.getEndTime().compareTo(second.getEndTime()) <= 0))
                    ||
                    (first.getStartTime().compareTo(second.getStartTime()) == 0) &&
                            (first.getEndTime().compareTo(second.getEndTime()) < 0));

    public Set<BusService> getAllServices() {
        return allServices;
    }


    public void offerService(BusService service) {

        //TODO use constant (maybe even predefined in java
        if (service.getTripDuration() > 3600) {
            return;
        }

        List<BusService> inefficientServices = new ArrayList();
        for (BusService bus : allServices) {
            if (moreEfficient.test(bus, service) ||sameGrotty.test(bus,service) ) {
                inefficientServices.add(bus);
            } else if (sameGrotty.test(service,bus) ||lessEfficient.test(bus, service)) {
                return;
            }
        }

        for (BusService inefficientService : inefficientServices) {
            System.out.println("inefficient services size: " + inefficientServices.size());
            allServices.remove(inefficientService);
        }

        allServices.add(service);

    }
}
