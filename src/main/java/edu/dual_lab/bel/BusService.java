package edu.dual_lab.bel;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Objects;

public class BusService implements Comparable<BusService>{

	private String name;
	private String startTime;
	private String endTime;
	private String originalString;
	private long tripDuration;

	@Override
	public String toString() {
		return originalString !=null? originalString: name+" "+startTime+" "+endTime;
	}


	public BusService(String name, String startTime, String endTime) {
		this.name = name;
		this.endTime = endTime;
		this.startTime = startTime;
		setTripDuration();
	}


	public String getName() {
		return name;
	}


	public String getStartTime() {
		return startTime;
	}


	public String getEndTime() {
		return endTime;
	}

	public void setOriginalString(String originalString) {
		this.originalString = originalString;
	}

	public long getTripDuration() {
		return tripDuration;
	}

	private void setTripDuration() {
		DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_TIME;
		LocalTime arrival = LocalTime.parse(this.endTime, formatter);
		LocalTime departure = LocalTime.parse(this.startTime, formatter);
		this.tripDuration = ChronoUnit.SECONDS.between(departure, arrival);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		BusService service = (BusService) o;
		return tripDuration == service.tripDuration &&
				Objects.equals(name, service.name) &&
				Objects.equals(startTime, service.startTime) &&
				Objects.equals(endTime, service.endTime);
	}

	@Override
	public int hashCode() {

		return Objects.hash(name, startTime, endTime, originalString, tripDuration);
	}

	public int compareTo(BusService service){
		return this.startTime.compareTo(service.getStartTime());
	}
}
