package edu.dual_lab.bel;

import java.io.IOException;

public class App {
    public static void main(String[] args) {
        try {
            String filePath = args[0];
            TimeTable timeTable = BloggersVilleBusServiceClient.getTimeTableFromFile(filePath);
            BloggersVilleBusServiceClient.writeToFileWithSameDir(filePath, timeTable);

        } catch (InvalidInputException | IOException | ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }

}

